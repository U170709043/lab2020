public class Rectangle{
    int sideA;
    int sideB;
    Point topLeft;
    public Rectangle(Point p,int A,int B){
        this.sideA = A;
        this.sideB = B;
        this.topLeft = p;
    }
    public int Area(){
        return this.sideA*this.sideB;   
    }
    public int Perimeter(){
        return this.sideA+this.sideB*2;
    }
    public Point[] corners(){
        Point[] points= new Point[4];
        Point topRight = new Point (topLeft.xCoord+this.sideA,topLeft.yCoord);
        Point botLeft = new Point (topLeft.xCoord,topLeft.yCoord-this.sideB);
        Point botRight = new Point (topRight.xCoord, botLeft.yCoord);
        points[0]=this.topLeft;
        points[1]=topRight;
        points[2]=botLeft;
        points[3]=botRight;
        return points;
    }
}
      
